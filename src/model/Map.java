/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;


/**
 *
 * @author Francky
 */
public class Map {
    
    private Object[][] map;
    private int width;
    private int height;
    
    public Map(int width,int height,int seed){
        
        this.width = width;
        this.height = height;
        
        this.map = new Object[width][height];
                
        for(int cptX = 0 ; cptX < width ; cptX++){
                        
            for(int cptY = 0 ; cptY < height ; cptY++){
                
                int randomRessourceValue = (int) (Math.random() * ( 100-seed - 0 ));
                
                RessourceType randomRessourceType;
                switch(randomRessourceValue){
                    case 0 :
                    case 1 :
                        randomRessourceType = RessourceType.GOLD;
                        break;
                    case 2 :
                    case 3 :
                    case 4 :
                    case 5 :
                        randomRessourceType = RessourceType.IRON;
                        break;
                    case 6 :
                        randomRessourceType = RessourceType.PLUTONIUM;
                        break;
                    default:
                        randomRessourceType = RessourceType.EMPTY;
                }
                
                this.map[cptX][cptY] = new Ressource(randomRessourceType,(int) (Math.random() * ( 20 - 10 )));
            }
        }
        
    }
    
    public Map placePeople(List<Chief> chiefs, List<Soldier> soldiers){
        
        //Cette boucle affecte une position aux chef dans la map
        for (Chief chief: chiefs) {

            Boolean posXYAreCorrect = false;
            while(!posXYAreCorrect){

                int x = (int) (Math.random() * ( this.width-1 - 0 ));
                int y = (int) (Math.random() * ( this.height-1 - 0 ));

                chief.setPosition(new Position(x, y));

                //Pour vérifier que l'on écrase pas un chef
                if(this.map[x][y].getClass() == Ressource.class){
                    posXYAreCorrect = true;
                    this.map[x][y] = chief;
                }
            
            }
            
        }
       
        //Cette boucle affecte une position aux soldats par rapport à l'emplacement de leurs chef
        for (Soldier soldier: soldiers) {

            Boolean posXYAreCorrect = false;
            while(!posXYAreCorrect){

                //On affect un X dans la map et pas sur la position du chef
                int x = 0;
                Boolean posXIsCorrect = false;
                while(!posXIsCorrect){
                    //Un nombre aléatoire entre -3 et 3
                    int randomNumber = (int) (Math.random() * ( 6 - 0 )) - 3;
                    //Un x aléatoir entre -3 et 3 par rapport au chef
                    x = soldier.getChief().getPosition().getX() + randomNumber;

                    //On ne sort pas de la map
                    if(x >= 0 && x < this.width){
                        //ce n'est pas la position du chef
                        if(x != soldier.getChief().getPosition().getX()){
                            posXIsCorrect = true;
                        }
                    }

                }

                //On affect un Y dans la map et pas sur la position du chef
                int y = 0;
                Boolean posYIsCorrect = false;
                while(!posYIsCorrect){
                   //Un nombre aléatoire entre -3 et 3
                   int randomNumber = (int) (Math.random() * ( 6 - 0 )) - 3;
                   //Un x aléatoir entre -3 et 3 par rapport au chef
                   y = soldier.getChief().getPosition().getY() + randomNumber;

                    //On ne sort pas de la map
                    if(y >= 0 && y < this.height){
                        //ce n'est pas la position du chef
                        if(y != soldier.getChief().getPosition().getY()){
                            posYIsCorrect = true;
                        }
                    }

                }

                //On ne tombe pas sur un joueur (mais on peux écraser une ressource)
                if(this.map[x][y].getClass() == Ressource.class){
                    posXYAreCorrect = true;
                    this.map[x][y] = soldier;
                    soldier.setPosition(new Position(x, y));
                }

            }

        }
        
        return this;
    }
    
    public Object[][] getMap(){
        return map;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
    
}
