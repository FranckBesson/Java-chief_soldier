/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Francky
 */
public enum RessourceType {
    EMPTY("Empty"),
    GOLD("Gold"),
    IRON("Iron"),
    PLUTONIUM("plutonium");
    
    private String name;
    
    RessourceType(String name){
        this.name = name;
    }
    
    @Override
    public  String toString(){
        return name;
    }
}
