/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Francky
 */
public abstract class Personnage {
    
    private Position position;
    private String name;
    private Map map;
    private ArrayList<Ressource> ressources;
    
    public Personnage(){
        this.ressources = new ArrayList<>();
        this.ressources.add(new Ressource(RessourceType.IRON, 0));
        this.ressources.add(new Ressource(RessourceType.GOLD, 0));
        this.ressources.add(new Ressource(RessourceType.PLUTONIUM, 0));
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }
    
    public void addRessource(RessourceType ressourceType, int number){
        for (Ressource ressource : this.ressources) {
            ressource.setNumber(ressource.getNumber()+number);
        }
    }
    
}
