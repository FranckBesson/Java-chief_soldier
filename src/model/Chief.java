/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Controler.Controler;
import Observer_Observable.MyObservable;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Francky
 */
public class Chief extends Personnage implements Runnable, Observer{
    
    private MyObservable soldiers;
    
    //The observer
    private Controler controler;
    
    public Chief(Controler controler){
        
        //par ce qu'il observe les soldats
        soldiers = new MyObservable();
        
        //Par ce qu'il est observé par le controler
        this.controler = controler;
        this.controler.addObserver(this);
        
    }
    
    public void orderToSolider(Object order){
        soldiers.setChanged();
        soldiers.notifyObservers(order);
    }
    
    //fonction pour que les observé s'ajoute à l'observer
    public void addObserver(Observer o){
        soldiers.addObserver(o);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        
        if(arg == "start"){
            new Thread(this).start();
            this.orderToSolider("start");
            this.orderToSolider(RessourceType.IRON);
        }
        
    }

    @Override
    public void run() {
        
    }
    
}
