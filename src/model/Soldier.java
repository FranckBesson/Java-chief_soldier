/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import static java.lang.Math.abs;

/**
 *
 * @author Francky
 */
public class Soldier extends Personnage implements Observer, Runnable{
    
    private Chief chief;
    volatile private RessourceType currentRessourceType;
    
    public Soldier(Chief chief){
        super();
        //par ce qu'il est observé par un chef
        this.chief = chief;
        chief.addObserver(this);
        
    }
    
    //fonction appelé par le chef
    public void update(Observable o, Object arg){
        
        //Il s'agit du signal start
        if(arg == "start"){
            new Thread(this).start();
        }
        
        //Il s'agit d'une ressource
        if(arg.getClass() == RessourceType.class){
            currentRessourceType = (RessourceType) arg;
            //System.out.println(arg.toString());
        }
        
    }
    
    @Override
    public void run(){
        
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
        while(true) {
            // traitement du thread
            try {
                Thread.sleep(100);
                //System.out.println("running for "+currentRessourceType);
                
                this.searchForRessource(super.getMap(), this.currentRessourceType);
                
            }catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        
    }
    
    //goToBottom : return moveTo(0,1);
    //goToTop : return moveTo(0, -1);
    //goToRigth : return moveTo(1, 0);
    //goToLeft : return moveTo(-1, 0);
    private boolean moveTo(int xOffset, int yOffset){
        
        try{
        //Si je ne quitte pas le plateau
        if((super.getPosition().getY()+yOffset < super.getMap().getHeight() && super.getPosition().getY()+yOffset >= 0) &&
           super.getPosition().getX()+xOffset < super.getMap().getWidth() && super.getPosition().getX()+xOffset >= 0){

            //Il n'y a rien
            if(super.getMap().getMap()[super.getPosition().getX()+xOffset][super.getPosition().getY()+yOffset].getClass() == Ressource.class && ((Ressource)super.getMap().getMap()[super.getPosition().getX()+xOffset][super.getPosition().getY()+yOffset]).getRessourceType() == RessourceType.EMPTY){

                this.moveTo(new Position(super.getPosition().getX()+xOffset,super.getPosition().getY()+yOffset));
                return true;

            }else{

                return false;

            }

        }else{

            return false;

        }
        }catch(Exception ex){
            System.err.println(ex);
            System.err.println("X offset : "+xOffset);
            System.err.println("Y offset : "+yOffset);
            System.err.println("X current"+super.getPosition().getX());
            System.err.println("Y current"+super.getPosition().getY());
        }
        return false;
    }
    
    private boolean goInX(Position position){
        
        //Il faut que j'aille à gauche (this.getX() --)
        if(super.getPosition().getX() - position.getX() > 0){

            //Go to left
            return moveTo(-1, 0);

        //Il faut que j'aille à droite (this.getX()++) 
        }else{

            //Go to rigth
            return moveTo(1, 0);

        }
                
    }
    
    private boolean goInY(Position position){
        
       //Il faut que j'aille en haut (this.GetY() --)
        if(super.getPosition().getY() - position.getY() > 0){

            //Go to top
            return moveTo(0, -1);

        //Il faut que j'aille en bas (this.GetY() ++)
        }else{

            //Go to bottom
            return moveTo(0,1);

        }
                
    }
    
    private void searchForRessource(Map map, RessourceType ressourceType) {
        
        //position que nous voulon atteindre
        Position position = this.findNerlyRessource(map, ressourceType, 0);
        
        //si nous sommes à coté
        if((abs(super.getPosition().getX() - position.getX()) == 1 && super.getPosition().getY() == position.getY()) ||
            (abs(super.getPosition().getY() - position.getY()) == 1 && super.getPosition().getX() == position.getX())){
            
            this.collectRessource(position);
            
        }else{
            
            //l'écart en X est plus important
            if( abs(super.getPosition().getX() - position.getX()) > abs(super.getPosition().getY() - position.getY()) ){
                
                if(!goInX(position))
                    if(!goInY(position)){
                        
                        //Il faut que j'aille en haut (this.GetY() --)
                        if(!(super.getPosition().getY() - position.getY() > 0)){

                            if(!moveTo(0, -1))//Top
                                if(!moveTo(0,1)){//Bot
                                    
                                    int randomNumber = (int) (Math.random() * ( 1 - 0 ));
                                    
                                    if(randomNumber == 0){
                                    
                                        if(!moveTo(-1, 0))//left
                                            moveTo(1,0);//right
                                    
                                    }else{
                                        
                                        if(!moveTo(1, 0))//right
                                            moveTo(-1,0);//left
                                        
                                    }
                                }

                        //Il faut que j'aille en bas (this.GetY() ++)
                        }else{

                            if(!moveTo(0,1))//Bot
                                if(!moveTo(0, -1)){//top
                                    
                                    int randomNumber = (int) (Math.random() * ( 1 - 0 ));
                                    
                                    if(randomNumber == 0){
                                    
                                        if(!moveTo(-1, 0))//left
                                            moveTo(1,0);//right
                                    
                                    }else{
                                        
                                        if(!moveTo(1, 0))//right
                                            moveTo(-1,0);//left
                                        
                                    }
                                }

                        }
                        
                    }
                        
            //L'écart en Y est plus important
            }else{
                
                if(!goInY(position))
                    if(!goInX(position)){
                        
                        //Il faut que j'aille à gauche (this.getX() --)
                        if(super.getPosition().getX() - position.getX() > 0){

                            if(moveTo(-1, 0))//left
                                if(moveTo(1,0)){//right
                                    
                                    int randomNumber = (int) (Math.random() * ( 1 - 0 ));
                                    
                                    if(randomNumber == 0){
                                    
                                        if(!moveTo(0, 1))//bot
                                            moveTo(0,-1);//top
                                    
                                    }else{
                                        
                                        if(!moveTo(0,-1))//top
                                            moveTo(0, 1);//bot
                                        
                                    }
                                }

                        //Il faut que j'aille à droite (this.getX()++) 
                        }else{

                            if(!moveTo(1, 0))//right
                                if(!moveTo(-1,0)){//left
                                    
                                    int randomNumber = (int) (Math.random() * ( 1 - 0 ));
                                    
                                    if(randomNumber == 0){
                                    
                                        if(!moveTo(0, 1))//bot
                                            moveTo(0,-1);//top
                                    
                                    }else{
                                        
                                        if(!moveTo(0,-1))//top
                                            moveTo(0, 1);//bot
                                        
                                    }
                                }
                            
                        }
                        
                    }
                
            }
            
        }
        
    }
    
    private Position findNerlyRessource(Map map, RessourceType ressourceType,int deep) {

        ArrayList<Position> positionList = new ArrayList<>();
        
        //coté du carré en fonction du deep 3 - 5 - 7 - 9 - 11 ... sois 3 + deep *2
        int squareSide = 3 + deep * 2;
        int valMax = (squareSide-1)/2;
        
        // La première ligne du haut
        int cptX = -valMax;
        int cptY = -valMax;
        for(; cptX <= valMax ; cptX++){
            
            //On reste dans les x de la map ?
            if(super.getPosition().getX()+cptX >= 0 && super.getPosition().getX()+cptX < map.getWidth()){
                
                //On reste dans les y de la map ?
                if(super.getPosition().getY()+cptY >= 0 && super.getPosition().getY()+cptY < map.getHeight()){
                
                    //Il y a une ressource à cette position ?
                    if(map.getMap()[super.getPosition().getX()+cptX][super.getPosition().getY()+cptY].getClass() == Ressource.class){
                        
                        Ressource ressource = (Ressource) map.getMap()[super.getPosition().getX()+cptX][super.getPosition().getY()+cptY];
                        
                       //Si la ressource est bien celle qui m'intéresse
                        if(ressource.getRessourceType() == this.currentRessourceType){
                            //je l'ajoute à la liste
                            positionList.add(new Position(super.getPosition().getX()+cptX, super.getPosition().getY()+cptY));
                        }
                        
                    }
                
                }
                
            }
            
        }
        
        // La seconde ligne du bas
        cptX = -valMax;
        cptY = valMax;
        for(; cptX <= valMax ; cptX++){
            
            //On reste dans les x de la map ?
            if(super.getPosition().getX()+cptX >= 0 && super.getPosition().getX()+cptX < map.getWidth()){
                
                //On reste dans les y de la map ?
                if(super.getPosition().getY()+cptY >= 0 && super.getPosition().getY()+cptY < map.getHeight()){
                
                    //Il y a une ressource à cette position ?
                    if(map.getMap()[super.getPosition().getX()+cptX][super.getPosition().getY()+cptY].getClass() == Ressource.class){
                        
                        Ressource ressource = (Ressource) map.getMap()[super.getPosition().getX()+cptX][super.getPosition().getY()+cptY];
                        
                       //Si la ressource est bien celle qui m'intéresse
                        if(ressource.getRessourceType() == this.currentRessourceType){
                            //je l'ajoute à la liste
                            positionList.add(new Position(super.getPosition().getX()+cptX, super.getPosition().getY()+cptY));
                        }
                        
                    }
                
                }
                
            }
            
        }
        
        // La première ligne de gauche
        cptX = -valMax;
        cptY = -valMax+1;
        for(; cptY <= valMax-1 ; cptY++){
            
            //On reste dans les x de la map ?
            if(super.getPosition().getX()+cptX >= 0 && super.getPosition().getX()+cptX < map.getWidth()){
                
                //On reste dans les y de la map ?
                if(super.getPosition().getY()+cptY >= 0 && super.getPosition().getY()+cptY < map.getHeight()){
                
                    //Il y a une ressource à cette position ?
                    if(map.getMap()[super.getPosition().getX()+cptX][super.getPosition().getY()+cptY].getClass() == Ressource.class){
                        
                        Ressource ressource = (Ressource) map.getMap()[super.getPosition().getX()+cptX][super.getPosition().getY()+cptY];
                        
                       //Si la ressource est bien celle qui m'intéresse
                        if(ressource.getRessourceType() == this.currentRessourceType){
                            //je l'ajoute à la liste
                            positionList.add(new Position(super.getPosition().getX()+cptX, super.getPosition().getY()+cptY));
                        }
                        
                    }
                
                }
                
            }
            
        }
        
        // La première ligne de gauche
        cptX = valMax;
        cptY = -valMax+1;
        for(; cptY <= valMax-1 ; cptY++){
            
            //On reste dans les x de la map ?
            if(super.getPosition().getX()+cptX >= 0 && super.getPosition().getX()+cptX < map.getWidth()){
                
                //On reste dans les y de la map ?
                if(super.getPosition().getY()+cptY >= 0 && super.getPosition().getY()+cptY < map.getHeight()){
                
                    //Il y a une ressource à cette position ?
                    if(map.getMap()[super.getPosition().getX()+cptX][super.getPosition().getY()+cptY].getClass() == Ressource.class){
                        
                        Ressource ressource = (Ressource) map.getMap()[super.getPosition().getX()+cptX][super.getPosition().getY()+cptY];
                        
                       //Si la ressource est bien celle qui m'intéresse
                        if(ressource.getRessourceType() == this.currentRessourceType){
                            //je l'ajoute à la liste
                            positionList.add(new Position(super.getPosition().getX()+cptX, super.getPosition().getY()+cptY));
                        }
                        
                    }
                
                }
                
            }
            
        }
        
        if(positionList.size() == 0){
            //si on dépasse pas la zone de recherche de la map
            if(squareSide < super.getMap().getHeight()*2){
                return this.findNerlyRessource(map, ressourceType, deep+1);
            }
            else{
                return this.chief.getPosition();
            }
        }else{
            
            //System.out.println("---- "+this.chief.getName()+" | "+this.name+" ----");
            //System.out.println("--("+this.x+";"+this.y+")--");
            //for (Position position: positionList) {
                //System.out.println("("+position.getX()+";"+position.getY()+")");
            //}
            //System.out.println("-----------------------------");
            
            if(positionList.size() > 1){
                
                int randomNumber = (int) (Math.random() * (positionList.size()-1 - 0));
                
                return positionList.get(randomNumber);
                
            }else{
                
                return positionList.get(0);
                
            }
            
        }
        
    }

    public Chief getChief() {
        return chief;
    }

    public void setChief(Chief chief) {
        this.chief = chief;
    }

    private void moveTo(Position position) {
        
        super.getMap().getMap()[super.getPosition().getX()][super.getPosition().getY()] = new Ressource(RessourceType.EMPTY,0);
        this.setPosition(position);
        super.getMap().getMap()[position.getX()][position.getY()] = this;
        
        
    }

    private void collectRessource(Position position) {
        if(super.getMap().getMap()[position.getX()][position.getY()].getClass() == Ressource.class){
            Ressource ressource = ((Ressource)super.getMap().getMap()[position.getX()][position.getY()]);
            
            ressource.setNumber(ressource.getNumber()-1);
            if(ressource.getNumber() == 0){
                super.getMap().getMap()[position.getX()][position.getY()] = new Ressource(RessourceType.EMPTY,0);
            }
            super.addRessource(ressource.getRessourceType(), 1);
        }
    }
    
}
