/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Francky
 */
public class Ressource {
    
    private RessourceType ressourceType;
    private int number;

    public Ressource(RessourceType ressourceType, int number){
        this.ressourceType = ressourceType;
        this.number = number;
    }    
    
    public RessourceType getRessourceType() {
        return ressourceType;
    }

    public void setRessourceType(RessourceType ressourceType) {
        this.ressourceType = ressourceType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        if(this.number > 0)
            this.number = number;   
    }
    
    
    
}
