/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import model.Map;

/**
 *
 * @author Francky
 */
public interface View {
    
    public void showMap(Map map);

    public void setMap(Map map);
    
}
