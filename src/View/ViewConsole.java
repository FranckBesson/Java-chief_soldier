/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Chief;
import model.Map;
import model.Ressource;
import model.RessourceType;
import model.Soldier;

/**
 *
 * @author Francky
 */
public class ViewConsole implements View, Runnable{
    
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    
    private Map map;

    public ViewConsole(){
        
    }
    
    @Override
    public void showMap(Map map){ 
        
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        
        for (Object[] subtab : map.getMap()) {
            
            for (Object object : subtab) {

                System.out.print("|");
                
                if(object.getClass() == Ressource.class){
                    
                    RessourceType ressourceType = ((Ressource)object).getRessourceType();
                    
                    if(ressourceType == RessourceType.EMPTY)
                        System.out.print(" ");
                    if(ressourceType == RessourceType.GOLD)
                        System.out.print("G");
                    if(ressourceType == RessourceType.IRON)
                        System.out.print("I");
                    if(ressourceType == RessourceType.PLUTONIUM)
                        System.out.print("P");
                
                }else if(object.getClass() == Chief.class){
                    System.out.print(ANSI_RED+"C"+ANSI_RESET);
                }else if(object.getClass() == Soldier.class){
                    System.out.print(ANSI_BLUE+"S"+ANSI_RESET);
                }
                    
            }
            
            System.out.print("|");
            System.out.println();
            
        }
            
    }

    @Override
    public void run() {
        
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
        while(true) {
            // traitement du thread
            try {
                Thread.sleep(100);
                //System.out.println("running for "+currentRessource);
                
                this.showMap(this.map);
                
            }catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }
    
}

