/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controler;

import Observer_Observable.MyObservable;
import View.View;
import View.ViewConsole;
import java.util.ArrayList;
import java.util.Observer;
import model.Chief;
import model.Map;
import model.Soldier;

/**
 *
 * @author Francky
 */
public class Controler {
    
    private View view;
    private MyObservable chiefs;
    
    public Controler(View view){
        
        //par ce qu'il observe les chefs
        chiefs = new MyObservable();
        
        this.view = view;
        ArrayList<Chief> chiefs = new ArrayList<>();
        ArrayList<Soldier> soldiers = new ArrayList<>();
        
        for(int cpt = 0 ; cpt < 20 ; cpt++){
            Chief chief_1 = new Chief(this);
            chiefs.add(chief_1);
            chief_1.setName("chief_"+cpt);
            Soldier solider_1 = new Soldier(chief_1);
            solider_1.setName("solider_1");
            soldiers.add(solider_1);
            Soldier solider_2 = new Soldier(chief_1);
            solider_2.setName("solider_2");
            soldiers.add(solider_2);
            Soldier solider_3 = new Soldier(chief_1);
            solider_3.setName("solider_3");
            soldiers.add(solider_3);
            
            /*
            Soldier solider_4 = new Soldier(chief_1);
            solider_4.setName("solider_4");
            soldiers.add(solider_4);
            
            Soldier solider_5 = new Soldier(chief_1);
            solider_5.setName("solider_5");
            soldiers.add(solider_5);
            
            Soldier solider_6 = new Soldier(chief_1);
            solider_6.setName("solider_6");
            soldiers.add(solider_6);
            
            Soldier solider_7 = new Soldier(chief_1);
            solider_7.setName("solider_7");
            soldiers.add(solider_7);
            
            Soldier solider_8 = new Soldier(chief_1);
            solider_8.setName("solider_8");
            soldiers.add(solider_8);
*/
            
        }  
        
        Map map = new Map(20,110,30);
        
        map.placePeople(chiefs, soldiers);
                
        //share map to poeple
        for (Chief chief: chiefs) {
            chief.setMap(map);
        }
        
        //share map to poeple
        for (Soldier soldier: soldiers) {
            soldier.setMap(map);
        }
        
        this.view.setMap(map);
        new Thread((ViewConsole)this.view).start();
                
        this.startAll();
    }
    
    public void startAll(){
        chiefs.setChanged();
        this.chiefs.notifyObservers("start");
    }
    
    public void stopAll(){
        chiefs.setChanged();
        this.chiefs.notifyObservers("stop");
    }
    
    //fonction pour que les observé s'ajoute à l'observer
    public void addObserver(Observer o){
        this.chiefs.addObserver(o);
    }
    
}
